%define enable_gpl  1
%define enable_non_free  1
%define buildyear %(echo %{version} | cut -b 7-10)
%define buildmonth %(echo %{version} | cut -b 11-12)
%define buildday %(echo %{version} | cut -b 13-14)
%define buildver %{version}
#% define buildver export-%{buildyear}-%{buildmonth}-%{buildday}
Name:          libav
Epoch:         1
Version:       11.7
Release:       1rktmb
Summary:       Hyper fast MPEG1/MPEG4/H263/RV and AC3/MPEG audio encoder
Group:         System/Libraries
Vendor:        openmamba
Distribution:  openmamba
Packager:      Silvan Calarco <silvan.calarco@mambasoft.it>
URL:           http://libav.org
Source:        http://libav.org/releases/libav-%{version}.tar.xz
Patch0:        libav-9.10-mixed-ffmpeg-2.2.patch
Patch3:        ffmpeg-0.5-x264.patch
Patch4:        libav-11.4-x86-asm-impossible-contraints-fix.patch
License:       LGPL, GPL
## AUTOBUILDREQ-BEGIN
BuildRequires: glibc-devel
BuildRequires: SDL-devel
BuildRequires: libX11-devel
BuildRequires: alsa-lib-devel
BuildRequires: bzip2-devel
BuildRequires: libdc1394-devel
# BuildRequires: faac-devel
BuildRequires: gsm-devel
# BuildRequires: libjack-devel
BuildRequires: lame-devel
BuildRequires: opencore-amr-devel
BuildRequires: openjpeg-devel
BuildRequires: schroedinger-devel
BuildRequires: speex-devel
BuildRequires: libtheora-devel
BuildRequires: libvdpau-devel
BuildRequires: libvorbis-devel
BuildRequires: x264-devel
BuildRequires: xvidcore-devel
# BuildRequires: libz-devel
## AUTOBUILDREQ-END
BuildRequires: a52dec-devel
BuildRequires: yasm-devel
BuildRequires: ldconfig
BuildRequires: libogg-devel
BuildRequires: faad2-devel
BuildRequires: libraw1394-devel
BuildRequires: openjpeg-devel
BuildRequires: orc-devel
# texi2html required
BuildRequires: tetex >= 3.0
BuildRoot:     %{_tmppath}/%{name}-%{version}-root

%description
FFmpeg is a very fast video and audio converter. It can also grab from a live audio/video source.
The command line interface is designed to be intuitive, in the sense that ffmpeg tries to figure out all the parameters, when possible.
You have usually to give only the target bitrate you want.
FFmpeg can also convert from any sample rate to any other, and resize video on the fly with a high quality polyphase filter.

%package -n libav-tools
Summary:       Hyper fast MPEG1/MPEG4/H263/RV and AC3/MPEG audio encoder
Group:         Applications/Multimedia
Provides:      ffmpeg-libav
Obsoletes:     ffmpeg-libav

%description -n libav-tools
FFmpeg is a very fast video and audio converter. It can also grab from a live audio/video source.
The command line interface is designed to be intuitive, in the sense that ffmpeg tries to figure out all the parameters, when possible.
You have usually to give only the target bitrate you want.
FFmpeg can also convert from any sample rate to any other, and resize video on the fly with a high quality polyphase filter.

%package -n libav-presets
Summary:   Preset configuration files used by libavcodec
Group:     System/Libraries
Provides:  ffmpeg-libav-presets
Obsoletes: ffmpeg-libav-presets

%description -n libav-presets
Preset configuration files used by libavcodec.

%package -n libswscale-libav
Summary:       Shared library part of ffmpeg
Group:         System/Libraries

%description -n libswscale-libav
Shared libswscale libraries part of ffmpeg.

%package -n libswscale-libav-devel
Summary:       Shared header files and static libraries part of ffmpeg
Group:         System/Libraries
Requires:      libswscale-libav = %{?epoch:%epoch:}%{version}-%{release}

%description -n libswscale-libav-devel
This is the common utility library from the ffmpeg project. 
It is required by all other ffmpeg libraries.

This package contains the header files and static libraries needed to compile applications or shared objects that use libswscale

%package -n libavcodec-libav
Summary:       Shared library part of ffmpeg
Group:         System/Libraries
Requires:      libav-presets

%description -n libavcodec-libav
Shared libavcodec libraries part of ffmpeg

%package -n libavcodec-libav-devel
Summary:       Shared header files and static libraries part of ffmpeg
Group:         System/Libraries
Requires:      libavcodec-libav = %{?epoch:%epoch:}%{version}-%{release}

%description -n libavcodec-libav-devel
This is the common utility library from the ffmpeg project. 
It is required by all other ffmpeg libraries.

This package contains the header files and static libraries needed to compile applications or shared objects that use libavcodec

%package -n libavdevice-libav
Summary:       Shared library part of ffmpeg
Group:         System/Libraries

%description -n libavdevice-libav
Shared libavdevice libraries part of ffmpeg

%package -n libavdevice-libav-devel
Summary:       Shared header files and static libraries part of ffmpeg
Group:         System/Libraries
Requires:      libavdevice-libav = %{?epoch:%epoch:}%{version}-%{release}

%description -n libavdevice-libav-devel
This is the common utility library from the ffmpeg project. 
It is required by all other ffmpeg libraries.

This package contains the header files and static libraries needed to compile applications or shared objects that use libavdevice.

%package -n libavformat-libav
Summary:       Shared library part of ffmpeg
Group:         System/Libraries

%description -n libavformat-libav
Shared libavformat libraries part of ffmpeg.

%package -n libavformat-libav-devel
Summary:       Shared header files and static libraries part of ffmpeg
Group:         System/Libraries
Requires:      libavformat-libav = %{?epoch:%epoch:}%{version}-%{release}

%description -n libavformat-libav-devel
This is the common utility library from the ffmpeg project. 
It is required by all other ffmpeg libraries.

This package contains the header files and static libraries needed to compile applications or shared objects that use libavformat

%package -n libavresample-libav
Summary:       libav resample library
Group:         System/Libraries

%description -n libavresample-libav
libav resample library.

%package -n libavresample-libav-devel
Summary:       Shared header files and static libraries part of libav
Group:         System/Libraries
Requires:      libavresample-libav = %{?epoch:%epoch:}%{version}-%{release}

%description -n libavresample-libav-devel
This is the common utility library from the ffmpeg project. 
It is required by all other libav libraries.

This package contains the header files and static libraries needed to compile applications or shared objects that use libavutil

%package -n libavutil-libav
Summary:       Shared library part of ffmpeg
Group:         System/Libraries

%description -n libavutil-libav
Shared library part of ffmpeg

%package -n libavutil-libav-devel
Summary:       Shared header files and static libraries part of ffmpeg
Group:         System/Libraries
Requires:      libavutil-libav = %{?epoch:%epoch:}%{version}-%{release}

%description -n libavutil-libav-devel
This is the common utility library from the ffmpeg project. 
It is required by all other ffmpeg libraries.

This package contains the header files and static libraries needed to compile applications or shared objects that use libavutil

%package -n libavfilter-libav
Summary:       Shared library part of ffmpeg
Group:         System/Libraries

%description -n libavfilter-libav
Shared library part of ffmpeg

%package -n libavfilter-libav-devel
Summary:       Shared header files and static libraries part of ffmpeg
Group:         System/Libraries
Requires:      libavfilter-libav = %{?epoch:%epoch:}%{version}-%{release}

%description -n libavfilter-libav-devel
This is the common utility library from the ffmpeg project.
It is required by all other ffmpeg libraries.

This package contains the header files and static libraries needed to compile applications or shared objects that use libavutil

%package -n libpostproc-libav
Summary:       Video postprocessing library from ffmpeg
Group:         System/Libraries

%description -n libpostproc-libav
FFmpeg is a very fast video and audio converter. It can also grab from a live audio/video source.

This package contains only ffmpeg's postproc post-processing library which other projects such as transcode may use. Install this package if you intend to use MPlayer, transcode or other similar programs.

%package -n libpostproc-libav-devel
Summary:       Video postprocessing library from ffmpeg
Group:         System/Libraries
Requires:      libpostproc = %{?epoch:%epoch:}%{version}-%{release}

%description -n libpostproc-libav-devel
FFmpeg is a very fast video and audio converter. It can also grab from a live audio/video source.

This package contains only ffmpeg's postproc post-processing headers and static libraries which other projects such as transcode may use. Install this package if you intend to use MPlayer, transcode or other similar programs.

%package apidocs
Group:         Documentation
Summary:       %{name} API documentation
Requires:      gtk-doc

%description apidocs
This package includes the %{name} API documentation.

%prep
%setup -q
%ifarch %{ix86}
%patch4 -p1
%endif
#%patch0 -p1
#rm -rf 'find ffmpeg-checkout-%{buildver} -name .svn | xargs'

%build
# configure not generated by autoconf
export CFLAGS="%{optflags} -fno-unit-at-a-time `pkg-config --cflags libopenjpeg1`"
set -o monitor
./configure \
    --prefix="%{_prefix}" \
    --libdir="%{_libdir}" \
	--shlibdir=%{_libdir} \
    --mandir="%{_mandir}" \
    --enable-libmp3lame \
    --enable-libvorbis \
    --enable-libtheora \
    --enable-libgsm \
    --enable-libdc1394 \
    --enable-libschroedinger \
    --enable-shared \
    --enable-pthreads \
    --enable-libopenjpeg \
    --enable-libspeex \
    --enable-swscale \
%if %enable_gpl
    --enable-gpl \
    --enable-version3 \
    --enable-libxvid \
    --enable-libx264 \
    --enable-libopencore-amrnb \
    --enable-libopencore-amrwb \
%if %enable_non_free
    --enable-nonfree 
%endif

%endif
#    --enable-libfaac \ 

#    --enable-postproc \
#    --enable-libdirac \
#    --enable-libfaadbin \
#    --enable-libfaad \
#    --disable-stripping \
#    --enable-avfilter-lavf \
#    --cpu=%{_target_cpu} \
#    --enable-swscale \
##    --incdir="%{_includedir}/ffmpeg" \
# FIXME:
# software scaler enabled   no
# Sun medialib support      no
# AVISynth enabled          no
# liba52 dlopened           no
# enable non free (libamrnb, libamrwb) License: unredistributable
# libamr-nb support         no
# libamr-wb support         no
# libgsm enabled            no
# libnut enabled            no

make -j 4

%install
[ "%{buildroot}" != / ] && rm -rf "%{buildroot}"

make install DESTDIR="%{buildroot}"


# create compat symlink
install -d %{buildroot}%{_libdir}/libavcodec
ln -s ../libavcodec.a %{buildroot}%{_libdir}/libavcodec/libavcodec.a

%clean
[ "%{buildroot}" != / ] && rm -rf "%{buildroot}"

%post -n libavcodec-libav -p /sbin/ldconfig
%postun -n libavcodec-libav -p /sbin/ldconfig

%post -n libavdevice-libav -p /sbin/ldconfig
%postun -n libavdevice-libav -p /sbin/ldconfig

%post -n libavformat-libav -p /sbin/ldconfig
%postun -n libavformat-libav -p /sbin/ldconfig

%post -n libavutil-libav -p /sbin/ldconfig
%postun -n libavutil-libav -p /sbin/ldconfig

%post -n libavfilter-libav -p /sbin/ldconfig
%postun -n libavfilter-libav -p /sbin/ldconfig

%post -n libpostproc-libav -p /sbin/ldconfig
%postun -n libpostproc-libav -p /sbin/ldconfig

%post -n libswscale-libav -p /sbin/ldconfig
%postun -n libswscale-libav -p /sbin/ldconfig

%files -n libav-tools
%defattr(-,root,root)
%{_bindir}/avconv
%{_bindir}/avplay
%{_bindir}/avprobe
%{_mandir}/man1/avconv.1*
%{_mandir}/man1/avplay.1*
%{_mandir}/man1/avprobe.1*
%doc CREDITS README

%files -n libav-presets
%defattr(-,root,root)
%{_datadir}/avconv/libvpx-*.avpreset
%{_datadir}/avconv/libx264-*.avpreset

%files -n libswscale-libav
%defattr(-,root,root)
%{_libdir}/libswscale.so.*

%files -n libswscale-libav-devel
%defattr(-,root,root)
%dir %{_includedir}/libswscale
%{_includedir}/libswscale/*.h
%{_libdir}/libswscale.a
%{_libdir}/libswscale.so
%{_libdir}/pkgconfig/libswscale.pc

%files -n libavcodec-libav
%defattr(-,root,root)
%{_libdir}/libavcodec.so.*

%files -n libavcodec-libav-devel
%defattr(-,root,root)
%dir %{_includedir}/libavcodec
%{_includedir}/libavcodec/*
%dir %{_libdir}/libavcodec
%{_libdir}/libavcodec/libavcodec.a
%{_libdir}/libavcodec.a
%{_libdir}/libavcodec.so
%{_libdir}/pkgconfig/libavcodec.pc

%files -n libavdevice-libav
%defattr(-,root,root)
%{_libdir}/libavdevice.so.*

%files -n libavdevice-libav-devel
%defattr(-,root,root)
%dir %{_includedir}/libavdevice
%{_includedir}/libavdevice/avdevice.h
%{_includedir}/libavdevice/version.h
%{_libdir}/libavdevice.a
%{_libdir}/libavdevice.so
%{_libdir}/pkgconfig/libavdevice.pc

%files -n libavformat-libav
%defattr(-,root,root)
%{_libdir}/libavformat.so.*

%files -n libavformat-libav-devel
%defattr(-,root,root)
%dir %{_includedir}/libavformat
%{_includedir}/libavformat/avformat.h
%{_includedir}/libavformat/avio.h
%{_includedir}/libavformat/version.h
%{_libdir}/libavformat.a
%{_libdir}/libavformat.so
%{_libdir}/pkgconfig/libavformat.pc

%files -n libavresample-libav
%defattr(-,root,root)
%{_libdir}/libavresample.so.*

%files -n libavresample-libav-devel
%defattr(-,root,root)
%dir %{_includedir}/libavresample
%{_includedir}/libavresample/avresample.h
%{_includedir}/libavresample/version.h
%{_libdir}/libavresample.a
%{_libdir}/libavresample.so
%{_libdir}/pkgconfig/libavresample.pc

%files -n libavutil-libav
%defattr(-,root,root)
%{_libdir}/libavutil.so.*

%files -n libavutil-libav-devel
%defattr(-,root,root)
%dir %{_includedir}/libavutil
%{_includedir}/libavutil/*.h
%{_libdir}/libavutil.a
%{_libdir}/libavutil.so
%{_libdir}/pkgconfig/libavutil.pc

%files -n libavfilter-libav
%defattr(-,root,root)
%{_libdir}/libavfilter.so.*

%files -n libavfilter-libav-devel
%defattr(-,root,root)
%dir %{_includedir}/libavfilter
%{_includedir}/libavfilter/*.h
%{_libdir}/libavfilter.a
%{_libdir}/libavfilter.so
%{_libdir}/pkgconfig/libavfilter.pc

#%files -n libpostproc-libav
#%defattr(-,root,root)
#%{_libdir}/libpostproc.so.*

#%files -n libpostproc-libav-devel
#%defattr(-,root,root)
#%dir %{_includedir}/libpostproc
#%{_includedir}/libpostproc/postprocess.h
#%{_libdir}/libpostproc.a
#%{_libdir}/libpostproc.so
#%{_libdir}/pkgconfig/libpostproc.pc

### %files apidocs
### %defattr(-,root,root)
### %dir %{_docdir}/libav
### %{_docdir}/libav/*

%changelog
* Mon Aug 24 2015 Silvan Calarco <silvan.calarco@mambasoft.it> 11.4-2mamba
- apidocs: don't require unexisting libav
- ffmpeg-libav: rename to libav-tools

* Mon Jun 08 2015 Automatic Build System <autodist@mambasoft.it> 11.4-1mamba
- automatic update by autodist

* Thu Mar 26 2015 Automatic Build System <autodist@mambasoft.it> 11.3-1mamba
- automatic update by autodist

* Sun Feb 15 2015 Automatic Build System <autodist@mambasoft.it> 11.2-1mamba
- automatic update by autodist

* Fri Dec 12 2014 Automatic Build System <autodist@mambasoft.it> 11.1-1mamba
- automatic update by autodist

* Thu Sep 18 2014 Automatic Build System <autodist@mambasoft.it> 10.5-1mamba
- automatic update by autodist

* Tue Jul 15 2014 Automatic Build System <autodist@mambasoft.it> 10.2-1mamba
- automatic update by autodist

* Tue Mar 25 2014 Silvan Calarco <silvan.calarco@mambasoft.it> 9.10-2mamba
- path to relax version requirements in pkg-config files to mix with ffmpeg (may not work, test with aubio 0.4.0)

* Fri Dec 27 2013 Automatic Build System <autodist@mambasoft.it> 9.10-1mamba
- automatic update by autodist

* Sat Sep 21 2013 Automatic Build System <autodist@mambasoft.it> 9.9-1mamba
- automatic update by autodist

* Fri Jul 26 2013 Automatic Build System <autodist@mambasoft.it> 9.8-1mamba
- automatic update by autodist

* Sat Jun 22 2013 Automatic Build System <autodist@mambasoft.it> 9.7-1mamba
- automatic version update by autodist

* Sun May 19 2013 Automatic Build System <autodist@mambasoft.it> 9.6-1mamba
- automatic update by autodist

* Tue Apr 02 2013 Automatic Build System <autodist@mambasoft.it> 9.4-1mamba
- automatic version update by autodist

* Thu Mar 07 2013 Automatic Build System <autodist@mambasoft.it> 9.3-1mamba
- automatic version update by autodist

* Sat Mar 02 2013 Automatic Build System <autodist@mambasoft.it> 9.2-1mamba
- automatic version update by autodist

* Sun Jan 27 2013 Automatic Build System <autodist@mambasoft.it> 9.1-1mamba
- automatic version update by autodist

* Wed Nov 07 2012 Silvan Calarco <silvan.calarco@mambasoft.it> 0.8.4-1mamba
- update to 0.8.4

* Wed Oct 26 2011 Silvan Calarco <silvan.calarco@mambasoft.it> 0.6.2-3mamba
- rename packages to libav*-libav and other changes to cohexist with ffmpeg libraries

* Thu Aug 04 2011 Silvan Calarco <silvan.calarco@mambasoft.it> 0.6.2-2mamba
- rebuilt with libx264 116

* Sun Mar 27 2011 Silvan Calarco <silvan.calarco@mambasoft.it> 0.6.2-1mamba
- update to 0.6.2

* Mon Oct 18 2010 Automatic Build System <autodist@mambasoft.it> 0.6.1-1mamba
- automatic update to 0.6.1 by autodist

* Tue Jun 22 2010 Automatic Build System <autodist@mambasoft.it> 0.6-1mamba
- automatic update to 0.6 by autodist

* Tue May 25 2010 Automatic Build System <autodist@mambasoft.it> 0.5.2-1mamba
- automatic update to 0.5.2 by autodist

* Tue Mar 16 2010 Automatic Build System <autodist@mambasoft.it> 0.5.1-2mamba
- automatic rebuild by autodist

* Fri Mar 05 2010 Automatic Build System <autodist@mambasoft.it> 0.5.1-1mamba
- automatic update to 0.5.1 by autodist

* Thu Nov 19 2009 Automatic Build System <autodist@mambasoft.it> 0.5.0.20091119-1mamba
- update to 0.5.snapshot20091119

* Wed Aug 12 2009 Automatic Build System <autodist@mambasoft.it> 0.5-3mamba
- automatic rebuild by autodist

* Fri Jul 10 2009 Automatic Build System <autodist@mambasoft.it> 0.5-2mamba
- automatic rebuild by autodist

* Mon Apr 06 2009 Silvan Calarco <silvan.calarco@mambasoft.it> 0.5-1mamba
- update to 0.5
- removed compatibility headers as complained by libxine 1.1.16.3

* Mon Dec 01 2008 Silvan Calarco <silvan.calarco@mambasoft.it> 20081113-5mamba
- remove provides for libffmpeg and libffmpeg-devel

* Thu Nov 20 2008 gil <puntogil@libero.it> 20081113-4mamba
- rebuilt with new libdc1394 2.0.2

* Thu Nov 20 2008 Silvan Calarco <silvan.calarco@mambasoft.it> 20081113-3mamba
- add obsoletes for libffmpeg and libffmpeg to all subpackages

* Wed Nov 19 2008 Silvan Calarco <silvan.calarco@mambasoft.it> 20081113-2mamba
- ffmpeg: don't obsolete libpostproc

* Fri Nov 14 2008 Silvan Calarco <silvan.calarco@mambasoft.it> 20081113-1mamba
- package renamed to ffmpeg; rename libraries to standard names and add proper provides and obsoletes for upgrade

* Wed Aug 20 2008 gil <puntogil@libero.it> 20080820-1mamba
- update to 20080820
- edit configure options
- added patch
- rebuilt with: libschroedinger, libamrnb, libamrwb, ffmpeg-checkout-snapshot.tar.bz2,
- enable non free (libamrnb, libamrwb) License: unredistributable 

* Mon Jun 02 2008 Silvan Calarco <silvan.calarco@mambasoft.it> 20080504-2mamba
- rebuilt with recent libdc1394

* Mon May 05 2008 Silvan Calarco <silvan.calarco@mambasoft.it> 20080504-1mamba
- update to 20080505

* Wed Apr 02 2008 Silvan Calarco <silvan.calarco@mambasoft.it> 20080402-1mamba
- update to 20080402

* Fri Feb 29 2008 Silvan Calarco <silvan.calarco@mambasoft.it> 20080229-1mamba
- update to 20080229
- set exported-snapshot as source instead of svn

* Sun Feb 03 2008 Silvan Calarco <silvan.calarco@mambasoft.it> 20080203-1mamba
- update to 20080203

* Thu Dec 13 2007 Silvan Calarco <silvan.calarco@mambasoft.it> 20071213-1mamba
- update to 20071213
- enabled pthread support
- changed package maintainer
- enabled support for libtheora and libx264

* Fri Jan 19 2007 Davide Madrisan <davide.madrisan@qilinux.it> 20070119-1qilnx
- updated to subversion 20070119
- dropped patch against CVE-2005-4048 (merged upstream)
- the shared libraries are now compiled with the -fPIC option: dropped patch

* Mon Dec 19 2005 Davide Madrisan <davide.madrisan@qilinux.it> 0.4.9-2qilnx
- fixed security issue CVE-2005-4048 (qibug#97)
- gcc4 fixes; compile the library with -fPIC
- HTML documentation moved to devel package

* Fri Jun 17 2005 Davide Madrisan <davide.madrisan@qilinux.it> 0.4.9-pre1-1qilnx
- update to version 0.4.9-pre1 by autospec

* Sun Oct 03 2004 Davide Madrisan <davide.madrisan@qilinux.it> 0.4.8-2qilnx
- specfile modified to match QiLinux standards
- library name modified (was ffmpeg)
- added a path to fix the broken configure check for the imlib2 library

* Mon Sep 13 2004 Matteo Bernasconi <voyagernm@virgilio.it> 0.4.8-1qilnx
- first build
